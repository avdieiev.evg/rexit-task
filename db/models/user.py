from datetime import date
from typing import Optional, Literal

from sqlalchemy import Column, String, Integer, Date
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import Session
from sqlalchemy.sql.ddl import CreateTable

from db.base import Base, engine


class Users(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, nullable=False)
    category = Column(String(255))
    firstname = Column(String(100))
    lastname = Column(String(100))
    email = Column(String(100))
    gender = Column(String(10))
    birth_date = Column(Date)

    @classmethod
    def get(
        cls, category: list[Optional[str]],
        gender: list[Optional[str]], birth_date: Optional[date],
        age: Optional[int], age_range_min: Optional[int],
        age_range_max: Optional[int], session: Session
    ) -> list['Users']:
        pass


Base.metadata.create_all(engine)
