import codecs
import csv
from datetime import datetime

from fastapi import APIRouter, Depends, UploadFile
from fastapi.openapi.models import Response
from sqlalchemy.orm import Session
from starlette import status
from starlette.responses import FileResponse

from config import RESPONSE_PATH
from db.base import get_session
from db.models.user import Users
from db_func import get_user
from schemas import UserFilterModel, UserModel

router = APIRouter()


@router.post('/client/create_from_csv')
async def save_data(
    csv_file: UploadFile,
    session: Session = Depends(get_session)
):
    csvreader = csv.DictReader(codecs.iterdecode(csv_file.file, 'utf-8'))
    for row in csvreader:
        session.add(Users(**row))  # type: ignore
    session.commit()

    return Response(
        status_code=status.HTTP_202_ACCEPTED,
        description='Data has been saved'
    )


@router.post('/client/export_to_csv')
def get_data(
    user: UserFilterModel,
    session: Session = Depends(get_session)
) -> FileResponse:
    db_models = get_user(
        **user.dict(), session=session
    )

    # TODO Not a best idea. Needs optimization
    to_csv = [UserModel.from_orm(model).dict() for model in db_models]

    # TODO add endpoint to delete responses if needed
    keys = to_csv[0].keys()
    filename = f'response_{datetime.now().strftime("%Y_%m_%d %H_%M_%S")}.csv'
    with open(f'{RESPONSE_PATH}/{filename}', 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(to_csv)
    return FileResponse(
        path=fr"{RESPONSE_PATH}/{filename}",
        filename=f"{filename}",
    )


