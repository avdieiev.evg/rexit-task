from datetime import date
from typing import Optional, Type

from sqlalchemy import func, text, extract
from sqlalchemy.orm import Session

from db.models import Users


def get_user(
        category: list[Optional[str]],
        gender: list[Optional[str]], birth_date: Optional[date],
        age: Optional[int], age_range_min: Optional[int],
        age_range_max: Optional[int], session: Session
) -> list[Type[Users]]:
    query = session.query(
        Users
    )
    if category:
        query = query.filter(
            Users.category.in_(category)
        )
    if gender:
        query = query.filter(
            Users.gender.in_(gender)
        )
    if birth_date:
        query = query.filter(
            Users.birth_date == birth_date
        )
    if age:
        query = query.filter(
            extract('YEAR', func.age(func.CURRENT_DATE(), Users.birth_date)) == age  # type: ignore
        )
    if age_range_min:
        query = query.filter(
            Users.birth_date < func.CURRENT_DATE() - text(f"INTERVAL '{age} years'")
        )
    if age_range_max:
        query = query.filter(
            Users.birth_date > func.CURRENT_DATE() - text(f"INTERVAL '{age} years'")
        )
    return query.all()
