import os

import uvicorn
from fastapi import FastAPI

from routes import router

from config import START_APP_KWARGS, RESPONSE_PATH

app = FastAPI()
app.include_router(router)

if __name__ == "__main__":
    if not os.path.exists(RESPONSE_PATH):
        os.makedirs(RESPONSE_PATH)
    uvicorn.run(**START_APP_KWARGS)
