### FastAPI-Postgres-docker-compose ##

* FastAPI app 
* PostgreSQL integration using SQLAlchemy
* Dockerfile and docker-compose integrations
* To .env you need to add DATABASE_URL variable with value of your local db url


### How to run this with Docker? ###

* Make sure you have docker installed and running on your machine
* Open the terminal to the docker-compose path and hit the following command - 
 ```
 MYCOMPUTER:FastAPI user$ docker-compose up --build
 ```
 