from datetime import date
from typing import Optional

from pydantic import BaseModel, Field, ConfigDict


class UserFilterModel(BaseModel):
    category: list[Optional[str]] = Field(None)
    gender: list[Optional[str]] = Field(None)
    birth_date: Optional[date] = Field(None)
    age: Optional[int] = Field(None)
    age_range_min: Optional[int] = Field(None)
    age_range_max: Optional[int] = Field(None)


class UserModel(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    category: str
    firstname: str
    lastname: str
    email: str
    gender: str
    birth_date: Optional[date]
